//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"sync"
	"time"

	"net/http"
	"strings"

	"github.com/oliveagle/jsonpath"

	"atos.pledger/k2p/common/logs"
	//log "atos.pledger/k2p/common/logs"
	"atos.pledger/k2p/k8s"
	"atos.pledger/k2p/kafka"
	"atos.pledger/k2p/templates"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	dto "github.com/prometheus/client_model/go"
	"github.com/prometheus/common/expfmt"
)

const pathLOG string = "K2P > "

type MonitoringEndPoint struct {
	// Business
	Domain   string
	Location string
	App      string
	Service  string
	// K8S
	Namespace string
	Pod       string
	Container string
	// Monitoring
	PortName               string
	ServiceMonitorSelector string
	ProxyNamespace         string
	ProxyApp               string
	ProxyPort              float64
	ProxyIP                string
	Interval               string
	Instance               string
	Job                    string
}

// InfraMananer represent the infrastructures to monitor
// They send metrics samples via kafka
type InfraManager struct {
	Domain   string // Domain of the infra or app owner. E.g: "mycompany.com"
	Location string // Human readable name of the location. E.g.: "Barcelona South"
	App      string
	//Lock  sync.Mutex
}

type MetricSamplesQueue struct {
	Queue []interface{}
	Lock  sync.Mutex
}

// InfraManagerCollector implements the Collector interface.
type InfraManagerCollector struct {
	InfraManager  *InfraManager
	Registry      *prometheus.Registry
	MetricSamples *MetricSamplesQueue
	Lock          sync.Mutex
}

//type InfraManagerCollectors []InfraManagerCollector

const K2P_QUEUE_LIMIT_DEFAULT int = 1024

//var reg *prometheus.Registry
//var queue []interface{}
//var collectors InfraManagerCollectors
var collectors map[string]*InfraManagerCollector
var collectorsLock sync.Mutex
var K2P_PROXY_APP string = "k2p"
var K2P_PROXY_NAMESPACE string = "default"
var K2P_PROXY_PORTNAME string = "metrics"
var K2P_PROXY_SERMONSEL string = ""
var K2P_PROXY_IP string = "127.0.0.1"
var K2P_PROXY_PORT string = "8080"
var K2P_SCRAPE_INTERVAL = "60s"
var K2P_QUEUE_LIMIT = K2P_QUEUE_LIMIT_DEFAULT
var K2P_PROXY_HEADLESS bool = false

//
func (ic *InfraManagerCollector) Describe(ch chan<- *prometheus.Desc) {
	logs.Debug("Prometheus call to Describe()...")
	prometheus.DescribeByCollect(ic, ch)
}

//
func (ic *InfraManagerCollector) Collect(ch chan<- prometheus.Metric) {
	logs.Debug("Prometheus call to Collect()... /" + ic.InfraManager.Domain +
		"/" + ic.InfraManager.Location + "/" + ic.InfraManager.App)
	// 1. Collect metrics from Kafka here (cache)
	//logs.Debug("Before queue locking...")
	//ic.MetricSamples.Lock.Lock()
	//defer ic.MetricSamples.Lock.Unlock()
	ic.Lock.Lock()
	defer ic.Lock.Unlock()
	//logs.Debug("After queue locking...")
	logs.Debug(fmt.Sprintf("Pending samples: %d", ic.MetricSamples.len()))
	for ic.MetricSamples.len() > 0 {
		//for len(ic.MetricSamples.Queue) > 0 {
		//ch <- ic.MetricSamples.peek().(prometheus.Metric)
		//logs.Debug(fmt.Sprintf("Sending metric: %v", ic.MetricSamples.peek().(prometheus.Metric)))
		prometric := ic.MetricSamples.peek().(prometheus.Metric)
		ch <- prometric
		ic.MetricSamples.dequeue()
		//logs.Debug(fmt.Sprintf("Sending metric: %v", prometric))
		mymetric := &dto.Metric{}
		prometric.Write(mymetric)
		//logs.Debug("Sending metric: " + proto.MarshalTextString(mymetric))
		logs.Debug(fmt.Sprintf("Sending metric: %s %v", prometric.Desc().String(), mymetric))
	}
	// 2. Create the Metric objects (const) with the original timestamp
	//for host, oomCount := range oomCountByHost {
	//	ch <- prometheus.MustNewConstMetric(
	//		oomCountDesc,
	//		prometheus.GaugeValue,
	//		float64(oomCount),
	//		host,
	//	)
	//}
	logs.Debug("Prometheus call to Collect() end.")
}

//
func NewInfraManager(domain string, location string, app string) (*InfraManager, *InfraManagerCollector) {
	logs.Debug("NewInfraManager... [" + domain + "].[" + location + "].[" + app + "]")
	//logs.Debug("Before collector locking...")
	collectorsLock.Lock()
	defer collectorsLock.Unlock()
	//logs.Debug("After collector locking...")
	if len(collectors) > 0 {
		if imc, ok := collectors[domain+location+app]; ok {
			return imc.InfraManager, imc
		}
	}
	im := &InfraManager{
		Domain:   domain,
		Location: location,
		App:      app,
	}
	reg := prometheus.NewPedanticRegistry()
	queue := make([]interface{}, 0)
	ms := &MetricSamplesQueue{Queue: queue}
	ic := &InfraManagerCollector{InfraManager: im, Registry: reg, MetricSamples: ms}
	if collectors == nil {
		collectors = make(map[string]*InfraManagerCollector)
	}
	collectors[domain+location+app] = ic
	//collectors = append(collectors, ic)
	prometheus.WrapRegistererWith(prometheus.Labels{"pledger": domain}, reg).MustRegister(ic)
	path := "/metrics/" + formatLabel(domain) + "/" + formatLabel(location) + "/" + formatLabel(app)
	http.Handle(path, promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	logs.Debug("NewInfraManager end.")
	return im, ic
}

//
func initEnv() {
	var ok bool
	K2P_PROXY_PORT, ok = os.LookupEnv("K2P_PROXY_PORT") // os.Getenv("K2P_PORT")
	if !ok {
		K2P_PROXY_PORT = "8080"
	}
	K2P_PROXY_NAMESPACE, ok = os.LookupEnv("K2P_PROXY_NAMESPACE")
	if !ok {
		K2P_PROXY_NAMESPACE = "default"
	}
	K2P_PROXY_APP, ok = os.LookupEnv("K2P_PROXY_APP")
	if !ok {
		K2P_PROXY_APP = "k2p"
	}
	K2P_PROXY_PORTNAME, ok = os.LookupEnv("K2P_PROXY_PORTNAME")
	if !ok {
		K2P_PROXY_PORTNAME = "metrics"
	}
	K2P_PROXY_SERMONSEL, ok = os.LookupEnv("K2P_PROXY_SERMONSEL")
	if !ok {
		K2P_PROXY_SERMONSEL = ""
	}
	K2P_PROXY_IP, ok = os.LookupEnv("K2P_PROXY_IP")
	if !ok {
		K2P_PROXY_IP = "127.0.0.1"
	}
	K2P_SCRAPE_INTERVAL, ok = os.LookupEnv("K2P_SCRAPE_INTERVAL")
	if !ok {
		K2P_SCRAPE_INTERVAL = "60s"
	}
	var tmp string
	var err error
	tmp, ok = os.LookupEnv("K2P_QUEUE_LIMIT")
	if !ok {
		K2P_QUEUE_LIMIT = K2P_QUEUE_LIMIT_DEFAULT
	} else {
		K2P_QUEUE_LIMIT, err = strconv.Atoi(tmp)
		if err != nil {
			K2P_QUEUE_LIMIT = K2P_QUEUE_LIMIT_DEFAULT
		}
	}
	tmp, ok = os.LookupEnv("K2P_PROXY_HEADLESS")
	if !ok {
		K2P_PROXY_HEADLESS = false
	} else {
		K2P_PROXY_HEADLESS, err = strconv.ParseBool(tmp)
		if err != nil {
			K2P_PROXY_HEADLESS = false
		}
	}
}

//
func main() {
	// Env vars init
	initEnv()
	// Start kafka listener
	//kafka.NewEventHandler(AddMetricsTextFormat)
	kafka.NewEventHandler(AddMetrics)
	//	K8S API client
	k8s.SetupConfig()
	// Testing
	//test()
	// Prometheus init
	reg := prometheus.NewPedanticRegistry()
	// Add the standard process and Go metrics to the custom registry.
	//reg.MustRegister(
	//	prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}),
	//	prometheus.NewGoCollector(),
	//)
	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	http.HandleFunc("/api/v1/metrics", processSamples)
	logs.Info(pathLOG + "Ready to connect ...")
	logs.Fatal(http.ListenAndServe(":"+K2P_PROXY_PORT, nil))
}

func (msq *MetricSamplesQueue) peek() interface{} {
	msq.Lock.Lock()
	defer msq.Lock.Unlock()
	return msq.Queue[0]
}

/*
func (msq *MetricSamplesQueue) poke(sample interface{}) []interface{} {
	msq.Lock.Lock()
	defer msq.Lock.Unlock()
	return msq.enqueue(sample)
}
*/

func (msq *MetricSamplesQueue) enqueue(sample interface{}) []interface{} {
	msq.Lock.Lock()
	defer msq.Lock.Unlock()
	if len(msq.Queue) >= K2P_QUEUE_LIMIT {
		//msq.dequeue()
		msq.Queue[0] = "" // nil
		msq.Queue = msq.Queue[1:]
	}
	msq.Queue = append(msq.Queue, sample)
	//logs.Debug("Enqueued: " + fmt.Sprintf("%v", msq.Queue) + " <- " + fmt.Sprintf("%v", sample))
	return msq.Queue
}

func (msq *MetricSamplesQueue) dequeue() []interface{} {
	msq.Lock.Lock()
	defer msq.Lock.Unlock()
	msq.Queue[0] = "" // nil
	msq.Queue = msq.Queue[1:]
	return msq.Queue
}

func (msq *MetricSamplesQueue) len() int {
	msq.Lock.Lock()
	defer msq.Lock.Unlock()
	return len(msq.Queue)
}

//
func AddMetricsTextFormat(msg string) {
	logs.Debug("AddMetricsTextFormat begin...")
	var parser expfmt.TextParser
	parsed, err := parser.TextToMetricFamilies(strings.NewReader(msg))
	if err != nil {
		//return nil, err
		panic(err)
	}
	//var result []*dto.MetricFamily
	//var result2 []string
	out := &bytes.Buffer{}
	for _, mf := range parsed {
		out.Reset()
		if _, err := expfmt.MetricFamilyToText(out, mf); err != nil {
			panic(err)
		}
		//result2 = append(result2, out.String())
		//result = append(result, mf)
		for _, metric := range mf.GetMetric() {
			var labels []string
			var values []string
			var domain, location, app, service string
			for _, label := range metric.GetLabel() {
				switch label.GetName() {
				case "domain":
					domain = label.GetValue()
				case "location":
					location = label.GetValue()
				case "app":
					app = label.GetValue()
				case "service":
					service = label.GetValue()
				}
				labels = append(labels, label.GetName())
				values = append(values, label.GetValue())
			}
			//fmt.Println(labels)
			//fmt.Println(mf.GetName(), mf.GetHelp(), mf.GetType().Enum(), mf.GetMetric()[0].GetLabel()[0].GetName())
			desc := prometheus.NewDesc(mf.GetName(), mf.GetHelp(), labels, nil)
			//fmt.Println(desc)
			var value float64
			var tipo prometheus.ValueType
			// Gauge, Counter, Histogram, Summary, Untyped
			//fmt.Println(mf.GetType())
			switch mf.GetType() {
			case dto.MetricType_GAUGE:
				value = metric.GetGauge().GetValue()
				tipo = prometheus.GaugeValue
			case dto.MetricType_COUNTER:
				value = metric.GetCounter().GetValue()
				tipo = prometheus.CounterValue
			default: // dto.MetricType_UNTYPED
				value = metric.GetUntyped().GetValue()
				tipo = prometheus.UntypedValue
			}
			timeStamp := time.Unix(0, metric.GetTimestampMs()*int64(time.Millisecond))
			if timeStamp.Unix() == 0 {
				timeStamp = time.Now()
			}
			constMetric := prometheus.NewMetricWithTimestamp(
				timeStamp,
				prometheus.MustNewConstMetric(desc, tipo, value, values...),
			)
			mymetric := &dto.Metric{}
			constMetric.Write(mymetric)
			//logs.Debug(proto.MarshalTextString(mymetric))
			//fmt.Println(constMetric)
			//var queue []interface{}
			logs.Debug(fmt.Sprintf("constMetric: %v %v %v", constMetric.Desc().String(), mymetric, metric))
			var found bool = false
			//for _, ic := range collectors {
			//	//logs.Debug("Looking in collector: " + ic.InfraManager.Domain)
			//	if ic.InfraManager.Domain == domain && ic.InfraManager.Location == location && ic.InfraManager.App == app {
			//collectorsLock.Lock()
			//defer collectorsLock.Unlock()
			if ic, ok := collectors[domain+location+app]; ok {
				ic.MetricSamples.enqueue(constMetric)
				found = true
			}
			//		break
			//	}
			//	found = false
			//}
			pns := K2P_PROXY_NAMESPACE
			smn := formatLabel(domain) + "-" + formatLabel(location) + "-" + formatLabel(app)
			//sn := smn + "-" + formatLabel(service)
			if !found {
				_, ic := NewInfraManager(domain, location, app)
				//fmt.Printf("%v %p\n", ic.MetricSamples.Queue, &ic.MetricSamples.Queue)
				//ic.MetricSamples = make([]interface{}, 0)
				ic.MetricSamples.enqueue(constMetric)
				//fmt.Printf("%v %p\n", ic.MetricSamples.Queue, &ic.MetricSamples.Queue)
				//logs.Debug(fmt.Sprintf("MetricSamples: %v[%d]", ic.MetricSamples[0].(prometheus.Metric).Desc(), len(ic.MetricSamples)))
				//for _, value := range ic.MetricSamples {
				//	logs.Debug(fmt.Sprintf("MetricSamples: %v", value.(prometheus.Metric).Desc()))
				//}
				//if !k8s.ExistsService(pns, sn) || !k8s.ExistsServiceMonitor(pns, smn) {
				if !k8s.ExistsService(pns, smn) || !k8s.ExistsServiceMonitor(pns, smn) {
					initPrometheusOperatorObjects(domain, location, app, service)
				}
			} else { // Previous error recovery?
				//if !k8s.ExistsService(pns, sn) || !k8s.ExistsServiceMonitor(pns, smn) {
				if !k8s.ExistsService(pns, smn) || !k8s.ExistsServiceMonitor(pns, smn) {
					initPrometheusOperatorObjects(domain, location, app, service)
				}
			}
		} // for samples
	} // for metrics types
	//fmt.Println(parsed)
	//fmt.Println(result, result2)
	//fmt.Print(out.String())
	logs.Debug("AddMetricsTextFormat end.")
}

//
func initPrometheusOperatorObjects(domain string, location string, app string, service string) {
	logs.Debug("initPrometheusOperatorObjects begin...")
	// A DNS-1035 label must consist of lower case alphanumeric characters or '-', start with an alphabetic character,
	// and end with an alphanumeric character (e.g. 'my-name',  or 'abc-123',
	// regex used for validation is '[a-z]([-a-z0-9]*[a-z0-9])?')
	if len(service) == 0 {
		service = app
	}
	port, _ := strconv.ParseFloat(K2P_PROXY_PORT, 64)
	mep := MonitoringEndPoint{
		Domain:                 formatLabel(domain),
		Location:               formatLabel(location),
		App:                    formatLabel(app),
		Service:                formatLabel(service),
		Namespace:              "",
		Pod:                    "",
		Container:              "",
		PortName:               K2P_PROXY_PORTNAME,
		ServiceMonitorSelector: K2P_PROXY_SERMONSEL,
		ProxyNamespace:         K2P_PROXY_NAMESPACE,
		ProxyApp:               K2P_PROXY_APP,
		ProxyPort:              port,
		ProxyIP:                K2P_PROXY_IP,
		Interval:               K2P_SCRAPE_INTERVAL,
		Instance:               "",
		Job:                    "",
	}

	pns := K2P_PROXY_NAMESPACE
	smn := mep.Domain + "-" + mep.Location + "-" + mep.App
	//sn := smn + "-" + mep.Service
	manifest := templates.NewServiceTemplate(mep, K2P_PROXY_HEADLESS)
	//k8s.CreateService(pns, sn, manifest)
	k8s.CreateService(pns, smn, manifest)
	//k8s.ExistsService(pns, sn)
	//k8s.DeleteService(pns, sn)
	manifest2 := templates.NewServiceMonitorTemplate(mep)
	k8s.CreateServiceMonitor(pns, smn, manifest2)
	//k8s.DeleteServiceMonitor(pns, smn)
	if K2P_PROXY_HEADLESS {
		manifest3 := templates.NewEndPointsTemplate(mep)
		//k8s.CreateEndpoints(pns, sn, manifest3)
		k8s.CreateEndpoints(pns, smn, manifest3)
		//k8s.DeleteEndpoints(pns, sn)
	}
	logs.Debug("initPrometheusOperatorObjects end.")
}

// '[a-z]([-a-z0-9]*[a-z0-9])?'
func formatLabel(label string) string {
	var result string
	result = strings.ToLower(label) // Lowercase
	var re = regexp.MustCompile(`([^a-z-0-9])`)
	result = re.ReplaceAllString(result, `-`) // Only alphanumeric or "-"
	re = regexp.MustCompile(`(^|[^a-z]*)([a-z]([-a-z0-9]*[a-z0-9])?)(.*|$)`)
	result = re.ReplaceAllString(result, `$2`)
	return result
}

/*
func dumpCollectors() {
	for _, ic := range collectors {
		logs.Debug("Looking in collector: " + ic.InfraManager.Domain + " " + fmt.Sprint(ic.MetricSamples.len()))
		//fmt.Printf("%v %p\n", ic.MetricSamples.Queue, &ic.MetricSamples.Queue)
		for _, value := range ic.MetricSamples.Queue {
			logs.Debug(fmt.Sprintf("MetricSamples: %v", value.(prometheus.Metric).Desc()))
		}
	}
}
*/
//
func processSamples(w http.ResponseWriter, req *http.Request) {
	logs.Debug(fmt.Sprintf("%v\n", req))
	if req.Method == "POST" {
		b, err := ioutil.ReadAll(req.Body)
		logs.Debug(fmt.Sprintf("%s\n", b))
		if err == nil {
			var json_data interface{}
			err := json.Unmarshal(b, &json_data)
			if err != nil { // Text format?
				go AddMetricsTextFormat(string(b))
			} else { // Custom Json format
				go AddMetricsJsonFormat(json_data)
			}
			w.Write([]byte("OK\n"))
			return
		} else {
			logs.Error(err)
			w.Write([]byte(err.Error()))
			return
		}
	}
	w.Write([]byte(http.StatusText(http.StatusNotImplemented)))
}

//
func AddMetrics(samples string) {
	var json_data interface{}
	err := json.Unmarshal([]byte(samples), &json_data)
	if err != nil { // Text format?
		go AddMetricsTextFormat(samples)
	} else { // Custom Json format
		go AddMetricsJsonFormat(json_data)
	}
}

/*
func test() {
	//
	   	text := `
	   # TYPE humidity_percent gauge
	   # HELP humidity_percent Humidity in %.
	   humidity_percent{domain="fill.com",location="outside",app="analytics"} 45.4 1635871101061
	   humidity_percent{domain="fill.com",location="inside",app="analytics"} 33.2
	   # TYPE temperature_kelvin gauge
	   # HELP temperature_kelvin Temperature in Kelvin.
	   # Duplicate metric:
	   temperature_kelvin{domain="holo.com",location="outside",app="VR/AR"} 265.3
	   # Missing location label (note that this is undesirable but valid):
	   temperature_kelvin 4.5
	   `
	//
	//NewInfraManager("edge", "Fill", "analytics")
	//NewInfraManager("edge2", "Holo", "VR/AR")
	//AddMetricsTextFormat(text)
	//dumpCollectors()
	//fmt.Println(reg)
	//
		port, _ := strconv.ParseFloat(K2P_PROXY_PORT, 64)
		mep := MonitoringEndPoint{
			Domain:         "fill",
			Location:       "inside",
			App:            "analytics",
			Service:        "queue",
			Namespace:      "core",
			Pod:            "",
			Container:      "",
			PortName:       "metrics",
			ProxyNamespace: "core",
			ProxyApp:       "pledger-k2p",
			ProxyPort:      port,
			ProxyIP:        "172.26.26.14",
			Interval:       "",
			Instance:       "",
			Job:            "",
		}

		ns := "core"
		smn := mep.Domain + "-" + mep.Location + "-" + mep.App
		sn := smn + "-" + mep.Service
		//manifest := templates.NewServiceTemplate(mep)
		//k8s.CreateService(ns, sn, manifest)
		//k8s.ExistsService(ns, sn)
		//k8s.DeleteService(ns, sn)
		//manifest2 := templates.NewServiceMonitorTemplate(mep)
		//k8s.CreateServiceMonitor(ns, smn, manifest2)
		//k8s.DeleteServiceMonitor(ns, smn)
		//manifest3 := templates.NewEndPointsTemplate(mep)
		//k8s.CreateEndpoints(ns, sn, manifest3)
		//k8s.DeleteEndpoints(ns, sn)
	//
}
*/

//
//	JsonPath: https://goessner.net/articles/JsonPath/
//  JsonPath online tester: https://jsonpath.com/
//
func AddMetricsJsonFormat(json_data interface{}) {
	logs.Debug("AddMetricsJsonFormat begin...")
	var out string
	var kind, domain, location, app, service string
	//
	kind = json_data.(map[string]interface{})["DocumentType"].(string)
	b, err := ioutil.ReadFile("./templates/" + kind + ".json")
	if err != nil {
		panic(err)
	}
	var json_schema interface{}
	err = json.Unmarshal(b, &json_schema)
	if err != nil {
		panic(err)
	}
	//
	value, err := jsonpath.JsonPathLookup(json_data, json_schema.(map[string]interface{})["domain"].(string))
	if err != nil {
		//panic(err)
		logs.Warn(err)
		value = []byte("")
	}
	domain = value.(string)
	value, err = jsonpath.JsonPathLookup(json_data, json_schema.(map[string]interface{})["location"].(string))
	if err != nil {
		//panic(err)
		logs.Warn(err)
		value = []byte("")
	}
	location = value.(string)
	//
	apps := json_schema.(map[string]interface{})["apps"].([]interface{})
	for _, appItem := range apps {
		var app_labels map[string]interface{}
		//value, err := jsonpath.JsonPathLookup(json_data, appItem.(map[string]interface{})["app"].(string))
		value, err = getValueFromSchema(appItem.(map[string]interface{})["app"].(string), json_data, 0)
		if err != nil {
			//panic(err)
			logs.Warn(err)
			value = []byte("")

		}
		app = value.(string)
		if appItem.(map[string]interface{})["labels"] != nil {
			app_labels = appItem.(map[string]interface{})["labels"].(map[string]interface{})
			for key := range app_labels {
				//value, err := jsonpath.JsonPathLookup(json_data, app_labels[key].(string))
				value, err = getValueFromSchema(app_labels[key].(string), json_data, 0)
				if err != nil {
					//panic(err)
					logs.Warn(err)
					value = []byte("")
				}
				//app_labels[key] = value.(string)
				app_labels[key] = castValueToString(value)
				logs.Debug("Set label " + key + "=" + app_labels[key].(string))
			} // for labels
		}
		//
		services := appItem.(map[string]interface{})["services"].([]interface{})
		for _, serviceType := range services {
			logs.Debug(fmt.Sprintf("Service Type: %v", serviceType))
			var collection []interface{}
			value, err = jsonpath.JsonPathLookup(json_data, serviceType.(map[string]interface{})["collection"].(string))
			if err != nil {
				panic(err)
				//logs.Warn(err)
				//value = []byte("")
			}
			var ok bool
			collection, ok = value.([]interface{})
			if !ok {
				collection = make([]interface{}, 0)
				collection = append(collection, value)
			}
			for indexSvc, serviceItem := range collection {
				logs.Debug(fmt.Sprintf("Collection[%d]: %v", indexSvc, serviceItem))
				var jsonPath string
				var serviceType_labels, service_labels map[string]interface{}
				//jsonPath = strings.Replace(serviceType.(map[string]interface{})["service"].(string), "*", strconv.Itoa(indexSvc), -1)
				//value, err := jsonpath.JsonPathLookup(json_data, jsonPath)
				value, err = getValueFromSchema(serviceType.(map[string]interface{})["service"].(string), json_data, indexSvc)
				if err != nil {
					//panic(err)
					logs.Warn(err)
					value = []byte("")
				}
				service = value.(string)
				logs.Debug("Service: " + service)
				service_labels = make(map[string]interface{})
				//service = serviceType.(map[string]interface{})["service"].(string)
				if serviceType.(map[string]interface{})["labels"] != nil {
					serviceType_labels = serviceType.(map[string]interface{})["labels"].(map[string]interface{})
					for key := range serviceType_labels {
						//value, err := jsonpath.JsonPathLookup(json_data, service_labels[key].(string))
						value, err = getValueFromSchema(serviceType_labels[key].(string), json_data, indexSvc)
						if err != nil {
							//panic(err)
							logs.Warn(err)
							value = []byte("")
						}
						service_labels[key] = castValueToString(value)
						logs.Debug("Set label " + key + "=" + service_labels[key].(string))
					} // for labels
				}
				//
				metrics := serviceType.(map[string]interface{})["metrics"].([]interface{})
				for _, metricItem := range metrics {
					var metricType_labels, metric_labels map[string]interface{}
					var metric_name, metric_timestamp string
					var metric_value float64
					//var jsonPath string
					//value, err := jsonpath.JsonPathLookup(json_data, metricItem.(map[string]interface{})["name"].(string))
					//if err != nil {
					//	panic(err)
					//	//return
					//}
					//metric_name = value.(string)
					metric_labels = make(map[string]interface{})
					metric_name = metricItem.(map[string]interface{})["name"].(string)
					if metricItem.(map[string]interface{})["labels"] != nil {
						metricType_labels = metricItem.(map[string]interface{})["labels"].(map[string]interface{})
						for key := range metricType_labels {
							//jsonPath = strings.Replace(metric_labels[key].(string), "*", strconv.Itoa(indexSvc), -1)
							//value, err := jsonpath.JsonPathLookup(json_data, jsonPath)
							value, err = getValueFromSchema(metricType_labels[key].(string), json_data, indexSvc)
							if err != nil {
								//panic(err)
								logs.Warn(err)
								value = []byte("")
							}
							//metric_labels[key] = value.(string)
							metric_labels[key] = castValueToString(value)
							logs.Debug("Set label " + key + "=" + metric_labels[key].(string))
						} // for labels
					}
					jsonPath = strings.Replace(metricItem.(map[string]interface{})["value"].(string), "*", strconv.Itoa(indexSvc), -1)
					value, err = jsonpath.JsonPathLookup(json_data, jsonPath)
					if err != nil {
						//panic(err)
						logs.Warn(err)
						//value = []byte("")
					}
					if value != nil {
						metric_value = value.(float64)
					}
					jsonPath = strings.Replace(metricItem.(map[string]interface{})["timestamp"].(string), "*", strconv.Itoa(indexSvc), -1)
					value, err = jsonpath.JsonPathLookup(json_data, jsonPath)
					if err != nil {
						//panic(err)
						logs.Warn(err)
						value = []byte("")
						//return
					}
					//logs.Debug(value.(string))
					ts, err := time.Parse(time.RFC3339, value.(string))
					//logs.Debug(ts.String())
					if err == nil {
						metric_timestamp = strconv.FormatInt(ts.UTC().UnixNano()/int64(time.Millisecond), 10)
						//logs.Debug(metric_timestamp)
						logs.Debug("TIMESTAMP: " + value.(string) + " => " + ts.String())
					} else {
						logs.Debug(err.Error())
					}
					//
					var output_labels string
					for k, v := range service_labels {
						if _, exists := metric_labels[k]; !exists {
							metric_labels[k] = v
						}
					}
					for k, v := range app_labels {
						if _, exists := metric_labels[k]; !exists {
							metric_labels[k] = v
						}
					}
					output_labels = fmt.Sprintf("domain=\"%s\",location=\"%s\",app=\"%s\",service=\"%s\"", domain, location, app, service)
					for k, v := range metric_labels {
						if len(output_labels) > 0 {
							output_labels += ","
						}
						output_labels = fmt.Sprintf("%s%s=\"%s\"", output_labels, k, v)
					}
					if len(metric_timestamp) > 0 {
						out = fmt.Sprintf("%s{%s} %f %s\n", metric_name, output_labels, metric_value, metric_timestamp)
					} else {
						out = fmt.Sprintf("%s{%s} %f\n", metric_name, output_labels, metric_value)
					}
					logs.Info(out)
					AddMetricsTextFormat(out)
				} // for metricItem
			} // for serviceItem
		} // for serviceType
	} // for appItem
	//AddMetricsTextFormat(out)
	logs.Debug("AddMetricsJsonFormat end.")
}

func getValueFromSchema(json_schema string, json_data interface{}, index int) (interface{}, error) {
	var jsonPath string
	var result interface{}
	var err error
	//logs.Debug("json_schema: " + json_schema)
	if strings.HasPrefix(json_schema, "$.") {
		jsonPath = strings.Replace(json_schema, "*", strconv.Itoa(index), -1)
		//logs.Debug("jsonPath: " + jsonPath)
		result, err = jsonpath.JsonPathLookup(json_data, jsonPath)
	} else {
		result = json_schema
	}
	return result, err
}

func castValueToString(value interface{}) string {
	var result string
	var ok bool
	switch v := value.(type) {
	case nil:
		result = ""
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		result = fmt.Sprintf("%d", v)
	case bool:
		result = fmt.Sprintf("%t", v)
	case float32, float64:
		result = fmt.Sprintf("%g", v)
	case string:
		result = value.(string)
	default:
		result, ok = value.(string)
		if !ok {
			//result = fmt.Sprintf("%v", v)
			result = ""
		}
	}
	return result
}
