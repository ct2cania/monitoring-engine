#!/bin/bash
#alias k='kubectl -n core'
kubectx kind-pledger
#kubectx kubernetes-antonio@kubernetes
kubectl -n core delete service/fill-uc3-edge-docker-analyse-media-consumption-pneumatic
kubectl -n core delete service/fill-uc3-edge-docker-chronograf
kubectl -n core delete service/fill-uc3-edge-docker-collect-brokertodbmanager-fast
kubectl -n core delete service/fill-uc3-edge-docker-collect-kafkatorabbitanalysis
kubectl -n core delete service/fill-uc3-edge-docker-collect-kafkatorabbittimestamp
kubectl -n core delete service/fill-uc3-edge-docker-collect-rabbittokafkafast
kubectl -n core delete service/fill-uc3-edge-docker-collect-rabbittokafkamonitor
kubectl -n core delete service/fill-uc3-edge-docker-collect-rabbittokafkatimestamp
kubectl -n core delete service/fill-uc3-edge-docker-cybernetics-analyze-frontend-base
kubectl -n core delete service/fill-uc3-edge-docker-grafana
kubectl -n core delete service/fill-uc3-edge-docker-influxdb
kubectl -n core delete service/fill-uc3-edge-docker-mongodb
kubectl -n core delete service/fill-uc3-edge-docker-monitoring-rabbitmq-docker
kubectl -n core delete service/fill-uc3-edge-docker-rabbitmq
kubectl -n core delete service/fill-uc3-edge-rabbitmq-exchanges
kubectl -n core delete service/fill-uc3-edge-rabbitmq-queues
kubectl -n core delete service/fill-uc3-edge-system-system

kubectl -n core delete servicemonitor fill-uc3-edge-docker
kubectl -n core delete servicemonitor fill-uc3-edge-rabbitmq
kubectl -n core delete servicemonitor fill-uc3-edge-system

