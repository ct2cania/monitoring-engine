#!/bin/bash
# Usage: ./testapijson.sh <proxy-port> <sample-file>
# Example: ./testapijson.sh 31010 docker.json
export K2P_PORT=8080
if [ "$#" -eq 2 ]; then
  K2P_PORT=$1 
  shift
fi
#sed s/TIMESTAMP/$(date +%s%3N)/g sample.txt | sed s/VALUE1/$(shuf -i0-100 -n1 -r -z)/g | curl -v -i -X POST -H "Content-Type: text/plain" localhost:$K2P_PORT/api/v1/metrics --data-binary @-
#sed "s/TIMESTAMP/$(date --rfc-3339=ns)/g" $1 | curl -v -i -X POST -H  "Content-Type: text/plain" localhost:$K2P_PORT/api/v1/metrics --data-binary @-
sed "s/TIMESTAMP/$(date --iso-8601=ns)/g" $1 | curl -v -i -X POST -H  "Content-Type: text/plain" localhost:$K2P_PORT/api/v1/metrics --data-binary @-
#curl -v -i -X POST -H "Content-Type: text/plain" localhost:$K2P_PORT/api/v1/metrics --data-binary @$1
#sed -i "s/$(date --rfc-3339=s)/TIMESTAMP/g" $1
