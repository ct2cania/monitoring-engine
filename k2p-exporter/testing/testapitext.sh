#!/bin/bash
export K2P_PORT=8080
if [[ ! -z "$1" ]]; then
  K2P_PORT=$1 
fi
sed s/TIMESTAMP/$(date +%s%3N)/g sample.txt | sed s/VALUE1/$(shuf -i0-100 -n1 -r -z)/g | curl -v -i -X POST -H "Content-Type: text/plain" localhost:$K2P_PORT/api/v1/metrics --data-binary @-
#curl -v -i -X POST -H "Content-Type: text/plain" localhost:8080/api/v1/metrics --data-binary @sample.txt

