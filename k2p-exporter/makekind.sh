#!/bin/sh
KCLUSTER_NAME=pledger
docker build -t k2p .
docker exec -it ${KCLUSTER_NAME}-control-plane crictl rmi docker.io/library/k2p
kind --name ${KCLUSTER_NAME} load docker-image k2p:latest
kubectl -n core delete deploy k2p
kubectl -n core apply -k kustomizer/overlays/localdev
sleep 10
kill $(ps aux | grep 'service/k2p' | grep kubectl | awk '{print $2}')
kubectl -n core port-forward service/k2p 31010:8080 > /dev/null 2>&1 &

