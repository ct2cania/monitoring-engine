//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//
package kafka

import (
	log "atos.pledger/k2p/common/logs"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "K2P > Kafka "

var KAFKA_METRIC_TOPIC string
var KAFKA_CLIENT string

//func init() {
//}

type PrometheusParser func(string)

func NewEventHandler(parser PrometheusParser) {
	var config = viper.New()
	config.SetEnvPrefix("K2P")
	config.AutomaticEnv()
	KAFKA_METRIC_TOPIC = config.GetString("KAFKA_METRIC_TOPIC")
	KAFKA_CLIENT = config.GetString("KAFKA_CLIENT")
	switch KAFKA_CLIENT {
	case "kafka2":
		New2(config)
	case "kafka":
		//kafka.New(config)
	default:
		return
	}
	go kafkaMetricEventLoop(parser)
}

func kafkaMetricEventLoop(parser PrometheusParser) {
	log.Println(pathLOG + "Starting to listen topic " + KAFKA_METRIC_TOPIC)
	for {
		var message string
		var err error
		switch KAFKA_CLIENT {
		case "kafka2":
			message, err = CheckForMessages2(KAFKA_METRIC_TOPIC)
		case "kafka":
		//message, err = kafka.CheckForMessages(KAFKA_VIOLATION_TOPIC)
		default:
			//break
			return
		}
		if err != nil {
			log.Error(pathLOG+"Error reading kafka event ...", err)
			continue
		}
		// Message processing ...
		log.Printf(pathLOG+"Event received from kafka: %s\n", message)
		// Message in Prometheus text-based format (https://prometheus.io/docs/instrumenting/exposition_formats/)
		// metric_name [
		//	"{" label_name "=" `"` label_value `"` { "," label_name "=" `"` label_value `"` } [ "," ] "}"
		//] value [ timestamp ]
		// Example:
		// # HELP http_requests_total The total number of HTTP requests.
		// # TYPE http_requests_total counter
		// http_requests_total{method="post",code="200"} 1027 1395066363000
		//AddMetricsTextFormat(message)
		parser(message)
	} // endless for
}
