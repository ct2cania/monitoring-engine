#!/bin/sh
#docker run -d --rm --name k2p pledger/k2p
#docker run --rm --name k2p -v $HOME/.kube:/root/.kube pledger/k2p
export K2P_PROXY_IP=$(ifconfig eth0 | grep broadcast | awk -e '{print $2}')
export K2P_PROXY_NAMESPACE="core"
export K2P_PROXY_HEADLESS="true"
export K2P_PROXY_SERMONSEL="release: r20-0-1"
#echo $K2P_PROXY_IP
go run k2p.go
