//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//

package k8s

import (
	"context"
	"flag"
	"path/filepath"
	"strconv"
	"time"

	// go get k8s.io/client-go@latest
	// Edit go.mod: k8s.io/client-go kubernetes-1.19.1
	// go mod tidy && go mod vendor

	log "atos.pledger/k2p/common/logs"

	v1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/deprecated/scheme"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"

	//monitoringclient "github.com/coreos/prometheus-operator/pkg/client/versioned"
	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	monitoringclient "github.com/prometheus-operator/prometheus-operator/pkg/client/versioned"
)

//
// Uncomment to load all auth plugins
// _ "k8s.io/client-go/plugin/pkg/client/auth"
//
// Or uncomment to load specific auth plugins
// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"

const pathLOG string = "K2P > K8SApiManager "

type KubeConfig struct {
	Name      string // Name of the cluster
	Server    string // https://{ip}:{port}
	Namespace string // "core"
	User      string // service account
	Token     string // pass
	//ServiceAPIPath string			// POST /api/v1/namespaces/{namespace}/services
	//ServiceMonitorAPIPath string	// POST /api/v1/namespaces/{namespace}/
}

var clientset *kubernetes.Clientset
var clientsetProOper monitoringclient.Interface

var CACHE_TIMEOUT int64 = 5 * 60 * 1000 // 5 minutes (ms)
var serviceCache map[string]time.Time
var serviceMonitorCache map[string]time.Time

func SetupConfig() {
	log.Info(pathLOG + "Creating K8S client...")
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil { // Outside of the cluster for development
		//panic(err.Error())
		var kubeconfig *string
		if home := homedir.HomeDir(); home != "" {
			kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
		} else {
			kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
		}
		flag.Parse()
		// use the current context in kubeconfig
		config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			panic(err.Error())
		}
	}
	// creates the clientset
	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	// create prometheus operator clientset
	clientsetProOper, err = monitoringclient.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	// cache init
	serviceCache = make(map[string]time.Time)
	serviceMonitorCache = make(map[string]time.Time)
	//return
}

func CreateService(namespace string, serviceName string, manifest string) {
	if ExistsService(namespace, serviceName) {
		log.Debug("Service " + serviceName + " already exists!")
		return
	}
	//log.Debug(manifest)
	var service *v1.Service
	decoder := serializer.NewCodecFactory(scheme.Scheme).UniversalDecoder()
	service = &v1.Service{}
	err := runtime.DecodeInto(decoder, []byte(manifest), service)
	if err != nil {
		panic(err)
	}
	//fmt.Printf("%v", service)
	service, err = clientset.CoreV1().Services(namespace).Create(context.TODO(), service, metav1.CreateOptions{
		//DryRun:       []string{"All"},
	})
	if err != nil {
		panic(err)
	}
	log.Debug(service.String())
}

func ExistsService(namespace string, serviceName string) bool {
	if getServiceCache(namespace, serviceName) {
		return true
	}
	_, err := clientset.CoreV1().Services(namespace).Get(context.TODO(), serviceName, metav1.GetOptions{})
	log.Debug("ExistsService: " + serviceName + " " + strconv.FormatBool(err == nil)) //err.Error())
	if err == nil {
		setServiceCache(namespace, serviceName)
	}
	return err == nil
}

func DeleteService(namespace string, serviceName string) bool {
	if !ExistsService(namespace, serviceName) {
		log.Debug("Service " + serviceName + " does not exist!")
		return false
	}
	err := clientset.CoreV1().Services(namespace).Delete(context.TODO(), serviceName, metav1.DeleteOptions{})
	log.Debug("DeleteService: " + serviceName + " " + strconv.FormatBool(err == nil))
	return err == nil
}

func setServiceCache(namespace string, serviceName string) {
	serviceCache[namespace+"_"+serviceName] = time.Now()
}

func getServiceCache(namespace string, serviceName string) bool {
	ts, ok := serviceCache[namespace+"_"+serviceName]
	if ok && ts.UnixMilli()+CACHE_TIMEOUT > time.Now().UnixMilli() {
		return true
	}
	delete(serviceCache, namespace+"_"+serviceName)
	return false
}

func CreateServiceMonitor(namespace string, serviceName string, manifest string) {
	if ExistsServiceMonitor(namespace, serviceName) {
		log.Debug("ServiceMonitor " + serviceName + " already exists!")
		return
	}
	log.Debug(manifest)
	var serviceMonitor *monitoringv1.ServiceMonitor
	decoder := serializer.NewCodecFactory(scheme.Scheme).UniversalDecoder()
	serviceMonitor = &monitoringv1.ServiceMonitor{}
	err := runtime.DecodeInto(decoder, []byte(manifest), serviceMonitor)
	if err != nil {
		panic(err)
	}
	//fmt.Printf("%v", serviceName)
	//serviceMonitor, err = clientset.CoreV1().Services(namespace).Create(context.TODO(), service, metav1.CreateOptions{
	//	DryRun: []string{"All"},
	//})
	serviceMonitor, err = clientsetProOper.MonitoringV1().ServiceMonitors(namespace).Create(context.TODO(), serviceMonitor, metav1.CreateOptions{})
	if err != nil {
		panic(err)
	}
	log.Debug(serviceMonitor.GetSelfLink())
}

func ExistsServiceMonitor(namespace string, serviceName string) bool {
	if getServiceMonitorCache(namespace, serviceName) {
		return true
	}
	//_, err := clientset.CoreV1().Services(namespace).Get(context.TODO(), serviceName, metav1.GetOptions{})
	_, err := clientsetProOper.MonitoringV1().ServiceMonitors(namespace).Get(context.TODO(), serviceName, metav1.GetOptions{})
	log.Debug("ExistsServiceMonitor: " + serviceName + " " + strconv.FormatBool(err == nil)) //err.Error())
	if err == nil {
		setServiceMonitorCache(namespace, serviceName)
	}
	return err == nil
}

func DeleteServiceMonitor(namespace string, serviceName string) bool {
	if !ExistsServiceMonitor(namespace, serviceName) {
		log.Debug("ServiceMonitor " + serviceName + " does not exist!")
		return false
	}
	//err := clientset.CoreV1().Services(namespace).Delete(context.TODO(), serviceName, metav1.DeleteOptions{})
	err := clientsetProOper.MonitoringV1().ServiceMonitors(namespace).Delete(context.TODO(), serviceName, metav1.DeleteOptions{})
	log.Debug("DeleteServiceMonitor: " + serviceName + " " + strconv.FormatBool(err == nil))
	return err == nil
}

func setServiceMonitorCache(namespace string, serviceName string) {
	serviceMonitorCache[namespace+"_"+serviceName] = time.Now()
}

func getServiceMonitorCache(namespace string, serviceName string) bool {
	ts, ok := serviceMonitorCache[namespace+"_"+serviceName]
	if ok && ts.UnixMilli()+CACHE_TIMEOUT > time.Now().UnixMilli() {
		return true
	}
	delete(serviceMonitorCache, namespace+"_"+serviceName)
	return false
}

func CreateEndpoints(namespace string, endpointsName string, manifest string) {
	if ExistsEndpoints(namespace, endpointsName) {
		log.Debug("Endpoints " + endpointsName + " already exists!")
		return
	}
	//log.Debug(manifest)
	var endpoints *v1.Endpoints
	decoder := serializer.NewCodecFactory(scheme.Scheme).UniversalDecoder()
	endpoints = &v1.Endpoints{}
	err := runtime.DecodeInto(decoder, []byte(manifest), endpoints)
	if err != nil {
		panic(err)
	}
	//fmt.Printf("%v", service)
	endpoints, err = clientset.CoreV1().Endpoints(namespace).Create(context.TODO(), endpoints, metav1.CreateOptions{
		//DryRun:       []string{"All"},
	})
	if err != nil {
		panic(err)
	}
	log.Debug(endpoints.String())
}

func ExistsEndpoints(namespace string, endpointsName string) bool {
	_, err := clientset.CoreV1().Endpoints(namespace).Get(context.TODO(), endpointsName, metav1.GetOptions{})
	log.Debug("ExistsEndpoints: " + endpointsName + " " + strconv.FormatBool(err == nil)) //err.Error())
	return err == nil
}

func DeleteEndpoints(namespace string, endpointsName string) bool {
	if !ExistsEndpoints(namespace, endpointsName) {
		log.Debug("Endpoints " + endpointsName + " does not exist!")
		return false
	}
	err := clientset.CoreV1().Endpoints(namespace).Delete(context.TODO(), endpointsName, metav1.DeleteOptions{})
	log.Debug("DeleteEndpoints: " + endpointsName + " " + strconv.FormatBool(err == nil))
	return err == nil
}
