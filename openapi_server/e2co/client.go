//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//

package e2co

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	"atos.pledger/monitoring-engine/common/logs"
	"atos.pledger/monitoring-engine/prometheus"
	"github.com/oliveagle/jsonpath"
)

const pathLOG string = "ME > E2CO "

type InfraInfo struct {
	infraId    string
	infraName  string
	namespace  string
	container  string
	prometheus string
	timestamp  int64
	sequence   int
	catalog    []string
}
type AppInfo struct {
	appId     string
	appName   string
	svcId     string
	svcName   string
	infraList map[string]InfraInfo
}

var appInfoCache map[string]AppInfo

var ME_E2CO_ENDPOINT string            // "http://localhost:8080"
var ME_E2CO_APP_PATH string            // "/api/v1/apps"
var ME_E2CO_INF_PATH string            // "/api/v1/ime/e2co"
var ME_E2CO_JSON_ORID string           // "$.ExtraContent.locations[0].idE2cOrchestrator"
var ME_E2CO_JSON_PROM string           // "$.Object.prometheusEndPoint"
var ME_DEFAULT_PROM string             // "http://localhost:9090"
var ME_E2CO_CACHE_TIMEOUT int64 = 3600 // Seconds
var ME_E2CO_NAME, ME_E2CO_EMAIL, ME_E2CO_PASSWORD string
var ME_E2CO_LOGIN_PATH string  // "/api/v1/public/login"
var ME_E2CO_SINGUP_PATH string // "/api/v1/public/singup"

var token string

func initEnv() {
	//var ok bool
	//ME_E2CO_ENDPOINT, ok = os.LookupEnv("ME_E2CO_ENDPOINT") // os.Getenv("ME_E2CO_ENDPOINT")
	//if !ok {
	//	ME_E2CO_ENDPOINT = "http://localhost:8080"
	//}
	ME_E2CO_ENDPOINT = lookupEnv("ME_E2CO_ENDPOINT", "http://localhost:8080")
	ME_E2CO_APP_PATH = lookupEnv("ME_E2CO_APP_PATH", "/api/v1/apps")
	ME_E2CO_INF_PATH = lookupEnv("ME_E2CO_INF_PATH", "/api/v1/ime/e2co")
	ME_E2CO_JSON_ORID = lookupEnv("ME_E2CO_JSON_ORID", "$.ExtraContent.locations[0].idE2cOrchestrator")
	ME_E2CO_JSON_PROM = lookupEnv("ME_E2CO_JSON_PROM", "$.Object.prometheusEndPoint")
	ME_DEFAULT_PROM = lookupEnv("ME_DEFAULT_PROM", "http://localhost:9090")
	var err error
	tmp, ok := os.LookupEnv("ME_E2CO_CACHE_TIMEOUT")
	if !ok {
		ME_E2CO_CACHE_TIMEOUT = 3600
	} else {
		ME_E2CO_CACHE_TIMEOUT, err = strconv.ParseInt(tmp, 10, 0) //.Atoi(tmp)
		if err != nil {
			ME_E2CO_CACHE_TIMEOUT = 3600
		}
	}
	ME_E2CO_NAME = lookupEnv("ME_E2CO_NAME", "guest")
	ME_E2CO_EMAIL = lookupEnv("ME_E2CO_EMAIL", "guest@gmail.com")
	ME_E2CO_PASSWORD = lookupEnv("ME_E2CO_PASSWORD", "guest")
	ME_E2CO_LOGIN_PATH = lookupEnv("ME_E2CO_LOGIN_PATH", "/api/v1/public/login")
	ME_E2CO_SINGUP_PATH = lookupEnv("ME_E2CO_SIGNUP_PATH", "/api/v1/public/signup")
}

func lookupEnv(name string, value string) string {
	//var result string
	//var ok bool
	result, ok := os.LookupEnv(name) // os.Getenv(name)
	if !ok {
		result = value
	}
	return result
}

func GetPrometheusEndpoint(svcId string, metric string, labels map[string]string) string {
	var result string
	logs.Debug(pathLOG + "GetPrometheusEndpoint(" + svcId + "," + metric + "," + fmt.Sprintf("%v", labels) + ")")
	//var prom_endpoint string
	var appInfo AppInfo
	var infraInfo InfraInfo
	if len(ME_E2CO_ENDPOINT) == 0 {
		initEnv()
	}
	if len(appInfoCache) == 0 {
		appInfoCache = make(map[string]AppInfo)
	}
	if len(appInfo.infraList) == 0 {
		appInfo.infraList = make(map[string]InfraInfo)
	}
	if _, ok := appInfoCache[svcId]; ok {
		logs.Debug("Checking cache hit for " + svcId)
		//logs.Debug(fmt.Sprintf("%v", appInfoCache[svcId]))
		for _, v := range appInfoCache[svcId].infraList {
			//logs.Debug(fmt.Sprintf("%v", v))
			if v.sequence == len(appInfoCache[svcId].infraList)-1 { // Latest infra
				if v.timestamp+ME_E2CO_CACHE_TIMEOUT > time.Now().Unix() { // Cache expired?
					//result = appInfoCache[svcId].infraList[k].prometheus
					appInfo := appInfoCache[svcId]
					result = filterByMetric(metric, &appInfo)
					logs.Info(pathLOG + "Prometheus endpoint (cached): " + result + " for appId " + svcId)
					return result
				}
			}
		}
	}
	logs.Debug("No valid cache entry for " + svcId)
	// 1. Get the Infra ID where the app in running now
	payload := GetE2coApp(svcId)
	appInfo.svcId = svcId
	value, err := jsonpath.JsonPathLookup(payload, "$.ExtraContent.apps[0].parent")
	if err == nil {
		appInfo.appId = value.(string)
	}
	appInfo.appName = "" // ?
	value, err = jsonpath.JsonPathLookup(payload, "$.ExtraContent.apps[0].name")
	if err == nil {
		appInfo.svcName = value.(string)
	}
	value, err = jsonpath.JsonPathLookup(payload, ME_E2CO_JSON_ORID)
	if err != nil {
		//panic(err)
		logs.Error(err)
		return ""
	}
	infraInfo.infraId = value.(string)
	appInfo.infraList[infraInfo.infraId] = infraInfo
	value, err = jsonpath.JsonPathLookup(payload, "$.ExtraContent.containers[0].name")
	if err == nil {
		infraInfo.container = value.(string)
	}
	value, err = jsonpath.JsonPathLookup(payload, "$.ExtraContent.locations[0].namespace")
	if err == nil {
		infraInfo.namespace = value.(string)
	}
	// 2. Get the Prometheus endpoint
	payload = GetE2coInfra(infraInfo.infraId)
	value, err = jsonpath.JsonPathLookup(payload, ME_E2CO_JSON_PROM)
	if err != nil {
		//panic(err)
		logs.Error(err)
		return ""
	}
	infraInfo.prometheus = value.(string)
	//logs.Debug("infraInfo.prometheus=" + infraInfo.prometheus)
	value, err = jsonpath.JsonPathLookup(payload, "$.Object.name")
	if err == nil {
		infraInfo.infraName = value.(string)
	}
	infraInfo.timestamp = time.Now().Unix()
	appInfo.infraList[infraInfo.infraId] = infraInfo
	if _, ok := appInfoCache[svcId]; !ok { // New svc
		appInfoCache[svcId] = appInfo
		infraInfo.sequence = 0
		//appInfoCache[svcId].infraList[infraInfo.infraId].sequence = 0
	} else { // New infra?
		appInfoCache[svcId].infraList[infraInfo.infraId] = infraInfo
		infraInfo.sequence = len(appInfoCache[svcId].infraList) - 1
		//appInfoCache[svcId].infraList[infraInfo.infraId].sequence = len(appInfoCache[svcId].infraList) - 1
	}
	result = appInfo.infraList[infraInfo.infraId].prometheus
	logs.Debug(pathLOG + "Prometheus endpoint (before filters): " + result + " for appId " + svcId)
	appInfo = appInfoCache[svcId]
	result = filterByMetric(metric, &appInfo)
	logs.Info(pathLOG + "Prometheus endpoint: " + result + " for appId " + svcId)
	return result
}

func GetE2coApp(svcId string) interface{} {
	if len(token) == 0 {
		token = loginE2co()
	}
	//curl -X GET "http://192.168.70.13:31000/api/v1/apps" -H  "accept: */*" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6Imd1ZXN0QGdtYWlsLmNvbSIsImV4cCI6MTYzOTc2MDM2OSwiaXNzIjoiQXV0aFNlcnZpY2UifQ.GRhtxPicyPySqgT9IW9_gNE4wVSngJGcEfyANPffAGw"
	//resp, err := http.Get(ME_E2CO_ENDPOINT + ME_E2CO_APP_PATH + "/" + svcId)
	//if err != nil {
	//	panic(err)
	//}
	//defer resp.Body.Close()
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	panic(err)
	//}
	body := httpGetWithBearerToken(ME_E2CO_ENDPOINT + ME_E2CO_APP_PATH + "/" + svcId)
	//fmt.Println(string(body))
	logs.Debug("GetE2coApp(" + svcId + ") -> " + string(body))
	var payload interface{}
	err := json.Unmarshal(body, &payload)
	if err != nil {
		panic(err)
	}
	return payload
}

func GetE2coInfra(infraId string) interface{} {
	if len(token) == 0 {
		token = loginE2co()
	}
	//resp, err := http.Get(ME_E2CO_ENDPOINT + ME_E2CO_INF_PATH + "/" + infraId)
	//if err != nil {
	//	panic(err)
	//}
	//defer resp.Body.Close()
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	panic(err)
	//}
	body := httpGetWithBearerToken(ME_E2CO_ENDPOINT + ME_E2CO_INF_PATH + "/" + infraId)
	//fmt.Println(string(body))
	logs.Debug("GetE2coInfra(" + infraId + ") -> " + string(body))
	var payload interface{}
	err := json.Unmarshal(body, &payload)
	if err != nil {
		panic(err)
	}
	return payload
}

func httpGetWithBearerToken(url string) []byte {
	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + token
	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	// add authorization header to the req
	req.Header.Add("Authorization", bearer)
	// Send req using http Client
	client := &http.Client{}
	logs.Debug("httpGetWithBearerToken header: " + bearer)
	logs.Debug("httpGetWithBearerToken request: " + url)
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	logs.Debug("httpGetWithBearerToken response: " + string(body))
	return body
}

func loginE2co() string {
	var result string
	//curl -X POST "http://192.168.70.13:31000/api/v1/public/signup" -H  "accept: */*" -H  "Content-Type: application/json" -d "{\"email\":\"guest@gmail.com\",\"password\":\"guest\",\"name\":\"guest\"}"
	//{"name": "guest","email": "guest@gmail.com","password": "$2a$14$SV8lh81hUpOCHIqyAD5JXedI1Ne/EteKnIrP1LTcVPCyqSdGAzc.S"}
	values := map[string]string{"email": ME_E2CO_EMAIL, "password": ME_E2CO_PASSWORD, "name": ME_E2CO_NAME}
	json_data, err := json.Marshal(values)
	if err != nil {
		panic(err)
	}
	logs.Debug("E2CO signup with: " + fmt.Sprintf("%v", values))
	logs.Debug("E2CO sigup request: " + ME_E2CO_ENDPOINT + ME_E2CO_SINGUP_PATH)
	resp, err := http.Post(ME_E2CO_ENDPOINT+ME_E2CO_SINGUP_PATH, "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		panic(err)
	}
	//var res map[string]interface{}
	//json.NewDecoder(resp.Body).Decode(&res)
	//defer resp.Body.Close()
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	panic(err)
	//}
	logs.Debug("E2CO signup response: " + fmt.Sprintf("%v", resp))
	//curl -X POST "http://192.168.70.13:31000/api/v1/public/login" -H  "accept: */*" -H  "Content-Type: application/json" -d "{\"email\":\"guest@gmail.com\",\"password\":\"guest\"}"
	//{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6Imd1ZXN0QGdtYWlsLmNvbSIsImV4cCI6MTYzOTc1ODE1NCwiaXNzIjoiQXV0aFNlcnZpY2UifQ.SJ9mAHQCk2FUD-v4T5wvNpirMd1Sl5yFCJyBTGyLPO0"}
	values = map[string]string{"email": ME_E2CO_EMAIL, "password": ME_E2CO_PASSWORD}
	json_data, err = json.Marshal(values)
	if err != nil {
		panic(err)
	}
	logs.Debug("E2CO login with: " + fmt.Sprintf("%v", values))
	logs.Debug("E2CO sigup request: " + ME_E2CO_ENDPOINT + ME_E2CO_LOGIN_PATH)
	resp, err = http.Post(ME_E2CO_ENDPOINT+ME_E2CO_LOGIN_PATH, "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		panic(err)
	}
	//var res map[string]interface{}
	//json.NewDecoder(resp.Body).Decode(&res)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	logs.Debug("E2CO login response: " + fmt.Sprintf("%v", resp))
	var payload interface{}
	err = json.Unmarshal(body, &payload)
	if err != nil {
		panic(err)
	}
	value, err := jsonpath.JsonPathLookup(payload, "$.token")
	if err == nil {
		result = value.(string)
	}
	logs.Debug("E2CO token: " + result)
	return result
}

func filterByMetric(metric string, appInfo *AppInfo) string {
	logs.Debug("filterByMetric( " + metric + "," + fmt.Sprintf("%v", appInfo) + ")")
	var result string = ME_DEFAULT_PROM
	var sequence int
	for k, v := range appInfo.infraList {
		var catalog []string
		var err error
		if v.timestamp+ME_E2CO_CACHE_TIMEOUT <= time.Now().Unix() || len(v.catalog) == 0 {
			logs.Debug("Checking metric " + metric + " metadata at " + v.prometheus)
			catalog, err = prometheus.ApiMetadata(v.prometheus, metric)
			if err != nil {
				panic(err)
			}
			logs.Debug("Catalog of " + metric + ": " + fmt.Sprintf("%v", catalog))
			//v.catalog = catalog
			//appInfo.infraList[k] = v
			infraInfo := appInfo.infraList[k]
			infraInfo.catalog = catalog
			appInfo.infraList[k] = infraInfo
			logs.Debug(fmt.Sprintf("Caching %v", appInfoCache[appInfo.svcId].infraList[k].catalog))
		} else {
			catalog = v.catalog
		}
		exists := false
		for m := range catalog {
			if catalog[m] == metric {
				exists = true
				break
			}
		}
		if exists && v.sequence >= sequence {
			sequence = v.sequence
			result = v.prometheus
		}
	}
	return result
}

func FilterbyLabels(query string, svcId string, metric string, labels map[string]string) string {
	var result string = query

	for k, v := range labels {
		logs.Debug("Filtering query by label: " + k)
		if len(v) > 0 { // Nothing to replace
			//labels[k] = "\"" + v + "\""
			continue
		}
		re := regexp.MustCompile(k + `=(,|})`)
		switch k {
		case "node":
			node := FilterByNode(svcId)
			labels[k] = node
			result = re.ReplaceAllString(result, k+"=\""+labels[k]+"\""+`$1`)
		case "pod":
			pod := FilterByPod(svcId)
			labels[k] = pod
			result = re.ReplaceAllString(result, k+"=~\""+labels[k]+"\""+`$1`)
		case "container":
			container := FilterByContainer(svcId)
			labels[k] = container
			result = re.ReplaceAllString(result, k+"=\""+labels[k]+"\""+`$1`)
		case "namespace":
			namespace := FilterByNameSpace(svcId)
			labels[k] = namespace
			result = re.ReplaceAllString(result, k+"=\""+labels[k]+"\""+`$1`)
		case "app":
			app := FilterByApp(svcId)
			labels[k] = app
			result = re.ReplaceAllString(result, k+"=\""+labels[k]+"\""+`$1`)
		case "service":
			service := FilterByService(svcId)
			labels[k] = service
			result = re.ReplaceAllString(result, k+"=\""+labels[k]+"\""+`$1`)
		default:
			// ?
		}
	}
	//for k, v := range labels {
	//	re := regexp.MustCompile(k + `=(,|})`)
	//	if len(v) > 0 { // Set labels resolved
	//		result = re.ReplaceAllString(result, k+"=~\""+v+"\""+`$1`) // Regular expression by default ?
	//	} else { // Delete labels not resolved
	//		result = re.ReplaceAllString(result, `$1`)
	//	}
	//}
	re := regexp.MustCompile(`([a-zA-Z0-9-:.]*)=([a-zA-Z0-9-:.]*)(,|})`)
	result = re.ReplaceAllString(result, `$1="$2"$3`)
	logs.Debug("Filtered query by labels: " + result)
	return result
}

func FilterByNode(svcId string) string {
	var result string = ""
	return result
}

func FilterByPod(svcId string) string {
	var result string = ""
	//result = "~" + appInfoCache[svcId].svcName + "-.*"
	result = appInfoCache[svcId].svcName + "-.*"
	return result
}

func FilterByApp(svcId string) string {
	var result string = ""
	result = appInfoCache[svcId].appName
	return result
}

func FilterByService(svcId string) string {
	var result string = ""
	result = appInfoCache[svcId].svcName
	return result
}

func FilterByNameSpace(svcId string) string {
	var result string = ""
	for _, v := range appInfoCache[svcId].infraList {
		if v.sequence == len(appInfoCache[svcId].infraList)-1 { // Latest infra
			result = v.namespace
		}
	}
	return result
}

func FilterByContainer(svcId string) string {
	var result string = ""
	for _, v := range appInfoCache[svcId].infraList {
		if v.sequence == len(appInfoCache[svcId].infraList)-1 { // Latest infra
			result = v.container
		}
	}
	return result
}
