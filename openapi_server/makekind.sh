#!/bin/sh
APP_NAME=monitoring-engine
docker build -t $APP_NAME .
docker exec -it kind-control-plane crictl rmi docker.io/library/$APP_NAME
kind load docker-image $APP_NAME:latest
kubectl -n core delete deploy $APP_NAME
kubectl -n core apply -k kustomizer/overlays/localdev
sleep 5
kill $(ps aux | grep 'service/$APP_NAME' | grep kubectl | awk '{print $2}')
kubectl -n core port-forward service/$APP_NAME 31011:4040 > /dev/null 2>&1 &

